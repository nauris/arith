<?php

class Expression {
    private $first = "";
    private $second = "";
    private $operator = "";
    private $result = "";
    private $lineLength = 0;

    function __construct($first, $second, $operator) {
        $this->first = $first;
        $this->second = $second;
        $this->operator = $operator;

        $this->calculate();
    }

    private function calculate() {
        switch ($this->operator) {
            case "+":
                $this->result = bcadd($this->first, $this->second);
                break;
            case "-":
                $this->result = bcsub($this->first, $this->second);
                break;
            case "*":
                $this->result = bcmul($this->first, $this->second);
                break;
        }

        $this->calculateLineLength();
    }

    private function calculateLineLength() {
        switch ($this->operator) {
            case "+":
                $this->lineLength = strlen($this->result) + 1;
                break;
            case "-":
                $this->lineLength = strlen($this->first) + 1;
                break;
            case "*":
                $this->lineLength = strlen($this->result);
                break;
        }
    }

    private function line($length) {
        return str_repeat("-", $length);
    }

    private function outputText($text) {
        $length = strlen($text);
        echo str_repeat(" ", $this->lineLength - $length);
        echo $text . "\n";
    }

    private function outputSimple() {
        $line = $this->line($this->lineLength);

        $this->outputText($this->first);
        $this->outputText($this->operator . $this->second);
        $this->outputText($line);
        $this->outputText($this->result);
    }

    private function outputComplex() {
        $second_array = str_split($this->second);

        $first_length = strlen($this->first);
        $second_length = sizeof($second_array);

        $first_line_length = $first_length > ($second_length + 1) ? $first_length : ($second_length + 1);

        $first_line = $this->line($first_line_length);
        $second_line = $this->line($this->lineLength);
        
        $this->outputText($this->first);
        $this->outputText($this->operator . $this->second);
        $this->outputText($first_line);

        for ($i = 0; $i < $second_length; $i++) {
            $single_operand = $second_array[$second_length - 1 - $i];
            $single_result = bcmul($this->first, $single_operand);
            $this->outputText($single_result . str_repeat(" ", $i));
        }

        $this->outputText($second_line);
        $this->outputText($this->result);
    }

    public function output() {
        if ($this->operator == "*" && strlen($this->second) > 1) {
            $this->outputComplex();
        }
        else {
            $this->outputSimple();
        }
    }
}

$expressions = [];

$expressions_count_input = readline();
$expressions_count = intval($expressions_count_input);

for ($i=0; $i < $expressions_count; $i++) {
    $expressions[] = readline();
}

echo "\n";

$matches = [];

for ($i=0; $i < $expressions_count; $i++) {
    preg_match_all('/\d+|\W/', $expressions[$i], $matches);

    $e = new Expression($matches[0][0], $matches[0][2], $matches[0][1]);
    $e->output();
    echo "\n";
}
